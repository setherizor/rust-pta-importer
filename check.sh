#! /bin/sh

cargo clippy --all -- -W clippy::all -W clippy::pedantic -W clippy::nursery

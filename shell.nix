{ pkgs ? import <nixpkgs> { } }:
with pkgs;
let
  fenix = import (fetchTarball "https://github.com/nix-community/fenix/archive/main.tar.gz") { };
in
mkShell rec {
  nativeBuildInputs = [
    pkg-config
    (with fenix; with stable; combine [
      rustc
      cargo
      cargo-sweep
      clippy
      bacon
      sccache
      cmake
      rustfmt
      rust-analyzer
      rust-src
      openssl
    ])
  ];

  RUSTC_WRAPPER = "${sccache}/bin/sccache";

  shellHook = ''
    export PATH="$PATH:~/.cargo/bin"
  '';
}

#! /bin/sh

cargo clean
cargo build --release
./patchelf.sh
scp ./target/release/rust-pta-importer lap:~/resilio-confs/resilio-sync/finance/.applications/bin/

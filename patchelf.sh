#! /usr/bin/env nix-shell
#! nix-shell -i bash -p bash patchelf

# Needed to run on non-nix computers (like popos)

patchelf --set-interpreter /lib64/ld-linux-x86-64.so.2 target/release/rust-pta-importer

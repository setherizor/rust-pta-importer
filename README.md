# Rust CSV -> PTA Importer

Requirements

- `ledger` CLI is installed
- Paisa installed in `~/Documents/paisa` and configured to include active
  journals

## How to Use

> This tool is for further automating CSV imports, PDF imports are not handled.

Optional params explanation (must include preceeding arguments)
```sh
./rust-pta-importer [IMPORT_DIR] [OUTPUT_FILE] --dryrun
```
> `OUTPUT_FILE` will use the first imported ledger for training the `predictAccount`` helper
> `--dryrun` must be the last argument
>
> You may optionally define environment varialbes to overrride the following environment variables
> - `FINANCE_PARENT_DIR` for the parent location of the finance directory
> - `PAISA_CONFIG_DIR` for the location of paisa's config file

### First Time Setup

1. Setup and explore [Paisa](https://paisa.fyi/)
2. Go to the `Ledger/Import` page, write & test import templates with a simple
   names
   1. if you want to change the name after the fact, Paisa's configuration page
      and scroll down
3. In the same folder as this program, create an `Imports` folder, and make
   subfolders with your template names from step 2

### Common Use

1. Download and place `.csv` files into the folder(s) matching the import
   template you want applied to them
2. Executing the program will do 3 things for each file
   1. Convert it into a `.ledger` using the specified template
   2. Append the converted content to the `recently_imported.ledger` file
   3. Rename with the imported date, and move the file to the completed folder
3. Work through the `recently_imported.ledger` file like a todo list, fixing
   problems and categorizing. Once you have a section, transaction, or the whole
   file "good", move that "verified" content to a more permant location

## Summarizing with [Ledger CLI](https://ledger-cli.org/doc/ledger3.html)

```sh
# Validate file and see overall changes from recent imports
ledger --strict -f recently_imported.ledger balance
```

## Useful Links

- [Paisa's Helpers Implementation](https://github.com/ananthakumaran/paisa/blob/704f25914bac51fa595e857eaec8f6f6399d155c/src/lib/template_helpers.ts)
- [Date Parsing](https://docs.rs/time/latest/time/struct.Date.html#method.parse)

use csv::ReaderBuilder;
use glob::{glob_with, MatchOptions};
use handlebars::{handlebars_helper, Handlebars, RenderError, ScopedJson};
use handlebars::{Context, Helper, HelperResult, Output, RenderContext, RenderErrorReason};
use lazy_static::lazy_static;
use natural::tf_idf::TfIdf;
use regex::Regex;
use serde_json::{json, Value};
use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::fs::OpenOptions;
use std::fs::{rename, File};
use std::io::{BufRead, BufReader, Write};
use std::path::Path;
use std::process::Command;
use std::vec;
use time::macros::format_description;
use time::Date;

static IMPORT_DIR_DEFAULT: &str = "Imports";
static COMPLETED_DIR: &str = ".processed";
static COMBINED_LEDGER: &str = "recently_imported.ledger";
static CLASSIFIER_CUTOFF: f32 = 0.085;

////////////////////////
// Helper Definitions //
////////////////////////

fn predict_account(
    h: &Helper,
    _: &Handlebars,
    context: &Context,
    _: &mut RenderContext,
    out: &mut dyn Output,
    account_classifiers: &HashMap<String, TfIdf>,
) -> HelperResult {
    let account_prefix = h
        .hash_get("prefix")
        .map(handlebars::PathAndJson::value)
        .ok_or(RenderErrorReason::ParamNotFoundForIndex(
            "predictAccount",
            0,
        ))?
        .as_str()
        .expect("Should have value");

    // Use context data or params to get a picturte of the transaction
    let query: String = if h.params().is_empty() {
        context.data().as_object().unwrap()["ROW"]
            .as_object()
            .unwrap()
            .values()
            .fold(String::new(), |a, x| a + x.as_str().unwrap() + " ")
    } else {
        h.params()
            .iter()
            .flat_map(|x| {
                if x.value().is_object() {
                    let l: Vec<&str> = x
                        .value()
                        .as_object()
                        .unwrap()
                        .values()
                        .map(|x| x.as_str().unwrap())
                        .collect();
                    l
                } else if x.value().is_null() {
                    vec![]
                } else {
                    vec![x.value().as_str().unwrap()]
                }
            })
            .fold(String::new(), |a, x| a + x.trim_start() + " ")
    };

    let query = query.trim().to_lowercase();

    let find_match = |query: &str| -> Vec<(String, f32)> {
        let mut matches: Vec<(String, f32)> = account_classifiers
            .iter()
            .map(|(name, classifier)| (name.to_owned(), classifier.get(query)))
            .filter(|(_, x)| !x.is_nan() && *x != 0.0)
            .collect();

        if !matches.is_empty() {
            matches.sort_by(|x, y| y.1.partial_cmp(&x.1).unwrap());
        }

        matches
    };

    let mut matches: Vec<(String, f32)> = find_match(&query);

    matches = matches
        .into_iter()
        .filter(|x| x.1 > CLASSIFIER_CUTOFF)
        .collect::<Vec<(String, f32)>>();

    // println!(
    //     "\nQuery: {query}\nFound: {} unfiltered matches",
    //     matches.len()
    // );

    if !account_prefix.is_empty() {
        matches = matches
            .into_iter()
            .filter(|x| x.0.starts_with(account_prefix))
            .collect::<Vec<(String, f32)>>();
    }

    // println!("Found: {} filtered matches", matches.len());

    // for x in &matches {
    //     println!("   {x:?}");
    // }

    let pick = if matches.is_empty() {
        if account_prefix.ends_with(':') {
            (account_prefix.to_owned() + "Unknown", -1.)
        } else {
            (account_prefix.to_owned() + ":Unknown", -1.)
        }
    } else {
        matches[0].clone()
    };

    // println!("Required prefix: {account_prefix:?}");
    // println!("Picked Account: {pick:?}");

    out.write(&pick.0)?;

    Ok(())
}

fn matcher(
    h: &Helper,
    _: &Handlebars,
    _: &Context,
    _: &mut RenderContext,
    out: &mut dyn Output,
) -> HelperResult {
    let mut match_options = h.hash().iter();
    let query = h.param(0).unwrap().value().as_str().unwrap();
    let pick = match_options
        .find(|(_, reg)| {
            let r = reg.value().as_str().unwrap();
            let reg = Regex::new(r).expect("Could not parse regex");
            reg.is_match(query)
        })
        .expect("Could not match");

    // println!("Match Options: {:#?}", h.hash());
    // println!("Query: {query:?}");
    // println!("    Picked Match: {pick:?}\n");

    out.write(pick.0)?;

    Ok(())
}

handlebars_helper!(negate: |x: Value| {
    if x.is_string() {
        x.as_str().expect("Could not parse signed number in negate").parse::<f64>().unwrap() * -1.
    } else {
        x.as_number().expect("Could not parse signed number in negate").as_f64().unwrap() * -1.
    }
});

handlebars_helper!(amount: |x: Value| {
    if x.is_number() {
        x.as_number().expect("could not extract number").as_f64().expect("coult not extract float")
    } else {
        let mut st = x.as_str().expect("could not extract string").trim().to_owned();

        // if st.is_empty() {
        //     return Ok(ScopedJson::from(Value::Number(serde_json::Number::from(0))))
        // }

        // Parse () wrapped numbers as negative
        let re = Regex::new(r"\(([\$]+(.+))\)").unwrap();
        if let Some(cap) = re.captures(&st) {
            st = "-$".to_owned() + &cap[2];
        }

        // Clean before parsing
        let re = Regex::new(r"[^0-9.-]").unwrap();
        st = re.replace_all(&st, "").to_string();

        st.parse::<f64>().unwrap()
    }
});

handlebars_helper!(replace: |input: Value, from: Value, to: Value|
    input.as_str().unwrap().replace(
        from.as_str().unwrap(),
        to.as_str().unwrap()
    )
);

handlebars_helper!(isDate: |v: Value, f: Value| {
    let parse_fmt = match f.as_str().unwrap() {
        "MM/DD/YYYY" => format_description!("[month]/[day]/[year]"),
        "M/D/YYYY" => format_description!("[month padding:none]/[day padding:none]/[year]"),
        "YYYY-MM-DD" => format_description!("[year]-[month]-[day]"),
        _ => panic!()
    };

    v.is_string() && Date::parse(v.as_str().expect("has string"), parse_fmt).is_ok()
});

// MM/DD/YYYY -> "[month]/[day]/[year]"
handlebars_helper!(date: |v: Value, f: Value| {
    let parse_fmt = match f.as_str().unwrap() {
        "MM/DD/YYYY" => format_description!("[month]/[day]/[year]"),
        "M/D/YYYY" => format_description!("[month padding:none]/[day padding:none]/[year]"),
        "YYYY-MM-DD" => format_description!("[year]-[month]-[day]"),
        _ => panic!()
    };

    let date_str = Date::parse(v.as_str().expect("has string"), parse_fmt).unwrap();

    date_str.format(format_description!("[year]/[month]/[day]")).unwrap()
});

handlebars_helper!(isBlank: |v: Value| v.as_str().unwrap_or_default().trim().eq(""));

// iterator helper
fn iter_to_alphamap<'a>(
    hashmap: &mut HashMap<String, String>,
    strings: impl Iterator<Item = &'a str>,
) {
    let mut x = 'A'..='Z';
    strings.for_each(|string| {
        hashmap.insert(x.next().unwrap().to_string(), string.to_owned());
    });
}

////////////////////////
//  Setup Predictors  //
////////////////////////

// ingest the ledger as a csv
// for each account create a classifier on its posting payee tokens
fn get_classifiers(incoming_file_path: &str) -> Result<HashMap<String, TfIdf>, Box<dyn Error>> {
    // Get ledger path from paisa's config
    let data: serde_yaml::Value = load_yaml()?;
    let relative_path = data["journal_path"].as_str().unwrap();

    // Command
    // ledger -f main.ledger csv --no-pager --format "%A,%P,%D,%t\n"

    let file_path = if incoming_file_path.is_empty() {
        env::var("FINANCE_PARENT_DIR").expect("No FINANCE_PARENT_DIR env variable set")
            + relative_path
    } else {
        // pull from first include
        let file = File::open(incoming_file_path)?;
        let reader = BufReader::new(file);
        reader
            .lines()
            .next()
            .expect("Output file did not include a base ledger")?
            .replace("include ", "")
    };

    if !Path::new(&file_path).exists() {
        println!("Error with training ledger: {file_path}");
        panic!("Training Ledger does not exist!")
    }

    let output = Command::new("ledger")
        .args([
            "-f",
            &file_path,
            "csv",
            "--no-pager",
            "--format",
            r"%A<%A<%P<%t\r\n",
            // "Expenses:", // take off expenses only training
        ])
        .output()?;

    // println!("{}", String::from_utf8_lossy(&output.stdout));

    let rdr = ReaderBuilder::new()
        .delimiter(b'<')
        .has_headers(false)
        .trim(csv::Trim::Fields)
        .from_reader(&output.stdout[..]);

    let mut map = HashMap::new();

    rdr.into_records().for_each(|result| {
        if result.is_err() {
            println!("Row data Error!!! {result:?}");
            println!("Row data Error {}", result.unwrap_err());
            return;
        }

        let row = result.unwrap();
        // println!("Row data {row:?}");
        let mut row = row.iter();

        if let Some(name) = row.next() {
            if !map.contains_key(name) {
                map.insert(name.to_owned(), TfIdf::new());
            }
            let thing = map.get_mut(name).unwrap();
            for cell in row {
                thing.add(&cell.to_lowercase());
            }
        }
    });

    // Add in components of the account name as tokens with a fairly high frequency
    for (key, classifier) in &mut map {
        // println!("key: {key:#?} val: {classifier:#?}");
        for _ in 0..5 {
            classifier.add(key);
        }
    }

    // Output classifier data
    // for x in map.clone() {
    //     println!("   {x:?}");
    // }

    Ok(map)
}

////////////////////////
//        Logic       //
////////////////////////

fn load_yaml() -> Result<serde_yaml::Value, Box<dyn Error>> {
    // Load and register templates from paisa's config
    let yaml_conf =
        env::var("PAISA_CONFIG_DIR").expect("No PAISA_CONFIG_DIR env variable set") + "/paisa.yaml";

    let file = File::open(yaml_conf)?;

    let data: serde_yaml::Value = match serde_yaml::from_reader(file) {
        Ok(data) => data,
        Err(err) => {
            eprintln!("There was an error parsing the YAML file {err}");
            std::process::exit(1);
        }
    };

    Ok(data)
}

fn load_templates(data: &serde_yaml::Value) -> Vec<(String, String)> {
    let configs = data["import_templates"]
        .as_sequence()
        .unwrap()
        .iter()
        .map(|f| {
            let x = f.as_mapping().unwrap();
            (x["name"].as_str().unwrap(), x["content"].as_str().unwrap())
        })
        .map(|x| (x.0.to_owned(), x.1.to_owned()))
        .collect::<Vec<(String, String)>>();

    configs
}

////////////////////////
//        Logic       //
////////////////////////

fn main() -> Result<(), Box<dyn Error>> {
    let mut hb = Handlebars::new();
    // Load and register templates from paisa's config
    let data: serde_yaml::Value = load_yaml()?;
    let config = load_templates(&data);
    let valid_templates = config.iter().map(|x| x.0.clone()).collect::<Vec<String>>();

    for (template_name, template) in config {
        hb.register_template_string(&template_name, template.clone())?;
        // println!("Loaded Template: {template_name}");
    }

    // register handlebars helpers to match paisa's functionality
    hb.register_helper("amount", Box::new(amount));
    hb.register_helper("negate", Box::new(negate));
    hb.register_helper("date", Box::new(date));
    hb.register_helper("isDate", Box::new(isDate));
    hb.register_helper("isBlank", Box::new(isBlank));
    hb.register_helper("replace", Box::new(replace));
    hb.register_helper("match", Box::new(matcher));

    lazy_static! {
        static ref ARGS: Vec<String> = env::args().collect();
        static ref OUTPUT_LEDGER: &'static str = if ARGS.len() > 2 {
            ARGS.get(2).map_or(COMBINED_LEDGER, |led| led)
        } else {
            COMBINED_LEDGER
        };
        static ref ACCOUNT_CLASSIFIERS: HashMap<String, TfIdf> =
            get_classifiers(*OUTPUT_LEDGER).unwrap();
    }

    // TODO: this is an odd approach
    hb.register_helper(
        "predictAccount",
        Box::new(
            |h: &Helper,
             r: &Handlebars,
             context: &Context,
             rc: &mut RenderContext,
             out: &mut dyn Output|
             -> HelperResult {
                predict_account(h, r, context, rc, out, &ACCOUNT_CLASSIFIERS)
            },
        ),
    );

    // println!();

    let options = MatchOptions {
        case_sensitive: false,
        require_literal_separator: false,
        require_literal_leading_dot: false,
    };

    let mut import_dir = IMPORT_DIR_DEFAULT;
    if ARGS.len() > 1 {
        if let Some(dir) = ARGS.get(1) {
            import_dir = dir;
        }
    }

    println!("Running Import in: {import_dir}");

    for entry in glob_with(&(import_dir.to_owned() + "/*/*.csv"), options)? {
        let file_path = entry?.display().to_string();
        let parts = file_path.splitn(3, '/').collect::<Vec<&str>>();
        let [_, template_name, file_short_name] = parts[..] else {
            todo!()
        };

        if !valid_templates.contains(&template_name.to_owned()) {
            println!("Invalid Template: '{template_name}' for {file_short_name}");
            continue;
        }

        println!("Ledgerizing '{file_path}'");

        // Read sheet data so its accessible in template through SHEET.0.A
        let rdr = ReaderBuilder::new()
            .flexible(true)
            .has_headers(false)
            .from_path(&file_path)?;

        let sheetdata = rdr
            .into_records()
            .enumerate()
            .map(|(index, result)| {
                if result.is_err() {
                    println!("Row data error {}", result.unwrap_err());
                    panic!("could not ingest the csv")
                }
                (index, result.unwrap())
            })
            .map(|(index, row)| {
                let mut hashmap: HashMap<String, String> = HashMap::new();
                iter_to_alphamap(&mut hashmap, row.into_iter());
                hashmap.insert("index".to_string(), index.to_string());

                hashmap
            })
            .collect::<Vec<_>>();

        let file_content = sheetdata
            .iter()
            .map(|rowdata| -> Result<String, RenderError> {
                // println!(
                //     "{:#?}",
                //     &json!({
                //         "ROW": &rowdata,
                //         "SHEET": "ignore"
                //     })
                // );
                hb.render(
                    template_name,
                    &json!({
                        "ROW": &rowdata,
                        "SHEET": &sheetdata
                    }),
                )
            })
            .map(Result::unwrap)
            .filter(|x| !x.trim().is_empty())
            .fold(String::new(), |a, x| a + x.trim_start() + "\n");

        if file_content.trim().is_empty() {
            println!("No transactions processed from file, Skipping: {file_path:#?}");
            continue;
        }

        // Ensure existance of combined output
        if !Path::new(*OUTPUT_LEDGER).exists() {
            let mut output = File::create(*OUTPUT_LEDGER)?;

            let main_ledger_path = env::var("FINANCE_PARENT_DIR")
                .expect("No FINANCE_PARENT_DIR env variable set")
                + data["journal_path"].as_str().unwrap();

            let main_ledger = format!("include {main_ledger_path}\n");

            output.write_all(main_ledger.as_bytes())?;
        }

        let mut output = OpenOptions::new()
            .create_new(false)
            .append(true)
            .open(*OUTPUT_LEDGER)
            .unwrap();

        let dt: time::OffsetDateTime = std::time::SystemTime::now().into();
        let timestamp = dt
            .format(format_description!("[year]-[month]-[day] [hour]:[minute]"))
            .unwrap();

        let ledger_header = format!(
            r#"
;##########################
;# Template: {template_name}
;# File Name: MARK: {file_short_name}
;# Imported: {timestamp}
;##########################

"#
        );

        output.write_all(ledger_header.as_bytes())?;
        output.write_all(file_content.as_bytes())?;

        // complete & move input

        if ARGS.len() > 1 && ARGS.last().unwrap().eq("--dryrun") {
            println!("Dryrun, not moving files to completed folder");
            continue;
        }

        let processed_template_path = format!("{import_dir}/{COMPLETED_DIR}/{template_name}");
        if !Path::new(&processed_template_path).exists() {
            std::fs::create_dir_all(processed_template_path.clone())?;
        }

        let used_path = processed_template_path + "/" + &timestamp + " " + file_short_name;
        rename(file_path, &used_path)?;
    }

    Ok(())
}
